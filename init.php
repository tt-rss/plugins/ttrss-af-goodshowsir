<?php
class Af_GoodShowSir extends Plugin {

	function about() {
		return array(1.0,
			"Fix image url in goodshowsir.co.uk RSS feed",
			"fox");
	}

	function init($host) {
		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
	}

	function hook_article_filter($article) {

		$doc = new DOMDocument();

		$found = false;

		if (!empty($article["content"]) && @$doc->loadHTML($article["content"])) {

			$xpath = new DOMXpath($doc);
			$images = $xpath->query('(//img[contains(@src, \'goodshowsir.co.uk\')])');

			foreach ($images as $img) {
				$src = $img->getAttribute("src");

				$tmp = trim($src);
				$tmp = str_replace("%20", "", $tmp);

				if ($tmp != $src) {
					$img->setAttribute("src", $tmp);

					$found = true;
				}
			}
		}

		if ($found) {
			$article["content"] = $doc->saveXML();
		}

		return $article;

	}


	function api_version() {
		return 2;
	}

}
?>
